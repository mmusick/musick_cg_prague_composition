[sig, fs] = audioread('prague_01.wav');

sig2 = sig(fs*30:fs*40);



sound(sig2, fs)




%%
shiftRatio = 0.5;

[p q] = rat((fs*shiftRatio)/fs);

sig3 = resample(sig2, p, q);


subplot(211);
[S, F, T] = spectrogram( sig2, 1024, 512, 2048, fs );
imagesc(abs(S));
set(gca,'YDir','normal');
legend = colorbar();

subplot(212);
[S, F, T] = spectrogram( sig3, 1024, 512, 2048, fs );
imagesc(abs(S));
set(gca,'YDir','normal');
legend = colorbar();

sound(sig3, fs)



%%


%% SPECTRAL SMEAR OF AUDIO FILE
%  No time shifting

filename = 'prague_01.wav';

overlapRatio = 0.5;
frameSize = 2^20;

sigInfo = audioinfo( filename );
[sig, fs] = audioread( filename );
bd = sigInfo.BitsPerSample;
% sig2 = sig(fs*30:fs*40);
sig2 = sig;

overlap = round(frameSize * overlapRatio );
window = hann(frameSize);
fftFrameSize = frameSize * 4;

sigBuff = buffer( sig2, frameSize, overlap, 'nodelay' );

numFrames = size(sigBuff, 2);

window = repmat(window, 1, numFrames);
sigBuff = sigBuff .* window;

SIG = fft(sigBuff, fftFrameSize );

SIG = abs(SIG);

sigBuff2 = ifft(SIG);


window = hann(fftFrameSize);
window = repmat(window, 1, numFrames);
sigBuff2 = sigBuff2 .* window;

% Create a new signal
newSig = zeros( ( (numFrames-1)*overlap + fftFrameSize ), 1);

for i = 1:numFrames
    
    frameStart = ((i-1)*overlap) + 1;
    frameEnd = frameStart + fftFrameSize - 1;
    
    newSig(frameStart:frameEnd) = newSig(frameStart:frameEnd) + sigBuff2(:,i);
    
end

plot( newSig );

pbSig = newSig(fs*60:fs*120);
pbSig = pbSig / max(abs( pbSig ));

player = audioplayer( pbSig, fs, bd );
play( player );

% stop(player)
% player.DeviceID



%% Spectral Time Shrink
% specShrink1, 9
% specShrink2, 10
% specShrink3, 7
% specShrink4, 4

filename = 'prague_04';
% filename = 'NYU_wsp_5';
frameSize = 2^16;
processRepeat = 10;
hopRatio = 0.5;
filewriteadd = 'specShrink2';

sigInfo = audioinfo( [filename, '.wav']  );
[sig, fs] = audioread( [filename, '.wav'] );
bd = sigInfo.BitsPerSample;
% sig2 = sig(fs*60:fs*(60*2));
sig2 = sig;

% Create Windows
timeWindow = tukeywin( frameSize, 0.1 );
fftWindow = tukeywin( frameSize, 0.95 );


for num = 1:processRepeat
    sigBuff = buffer( sig2, frameSize, 0, 'nodelay' );

    numFrames = size(sigBuff, 2);   
   
    window1 = repmat(timeWindow, 1, numFrames);
    sigBuff = sigBuff .* window1;
    
    SIG = fft(sigBuff);
    SIG = abs(SIG);
    sigBuff2 = ifft(SIG);   
    
    window2 = repmat(fftWindow, 1, numFrames);
    sigBuff2 = sigBuff2 .* window2;

    hop = round(frameSize * hopRatio);
    % Create a new signal
    newSig = zeros( ( (numFrames-1)*hop + frameSize ), 1);
    for i = 1:numFrames

        frameStart = ((i-1)*hop) + 1;
        frameEnd = frameStart + frameSize - 1;
        frame = sigBuff2(:,i);
        frame = frame / (max(abs(frame)));
        shift = (mean(frame)/2)
        frame = frame - shift;


        newSig(frameStart:frameEnd) = newSig(frameStart:frameEnd) + frame;

    end

%     sig2 = filter([0.925417400953572, 0.925417400953572, 0, 1.0, 0.850834801907143, 0], 1, sig2);
    
    sig2 = newSig;

    plot( newSig );

end


newSig = newSig / max(abs( newSig ));

pbSig = newSig;
player = audioplayer( pbSig, fs, bd );
play( player );

writename = [filename, filewriteadd, '.wav'];
audiowrite( writename, newSig, fs, 'BitsPerSample', 24 );

%%
stop( player );
